'use strict';

var App = angular.module('RSSFeedApp', []);

App.controller("FeedCtrl", ['$scope', 'FeedService', function ($scope, Feed) {
    $scope.feeds = new Array();
    $scope.rssFeedsSources = FeedsSource.feeds;
    $scope.rssFeedsConfig = FeedsSource.config;
    $scope.linkDefs = FeedsSource.links;
    $scope.ggStatsDefs = FeedsSource.ggStats;
    $scope.tryCount = new Array();
    $scope.loadFeed = function (idx, feedDef) {
        if (isNaN($scope.tryCount[idx])){
            $scope.tryCount[idx] = 0;
        }
        Feed.requestFeed(feedDef.url).then(function (res) {
            $('#feed-panel-body-loaded-' + idx).hide();
            $('#feed-panel-body-error-' + idx).hide();
            $('#feed-panel-body-loading-' + idx).show();
            if (res.data.query.results) {
                $scope.feeds[idx] = $scope.normalizeFeed(res.data.query.results);
                $('#feed-panel-body-loading-' + idx).hide();
                $('#feed-panel-body-error-' + idx).hide();
                $('#feed-panel-body-loaded-' + idx).show();
            } else {
                if ($scope.tryCount[idx]++ > FeedsSource.config.maxRetryCount){
                    $('#feed-panel-body-loading-' + idx).hide();
                    $('#feed-panel-body-error-' + idx).show();
                    $('#feed-panel-body-loaded-' + idx).hide();
                } else {
                    setTimeout(function(){
                        $scope.loadFeed(idx, feedDef);
                    }, FeedsSource.config.retryDelay);
                }
            }
        });
    };
    $scope.clearDesc = function (text) {
        var txt = text ? String(text).replace(/<[^>]+>/gm, '').trim() : 'Brak opisu';
        if (txt) {
            return txt.trim();
        }
        return 'Brak opisu';
    };
    $scope.normalizeFeed = function (data) {
        var arr = new Array();
        if (data.item) {
            for (var i = 0; i < data.item.length; i++) {
                arr[i] = {
                    title: data.item[i].title,
                    link: data.item[i].link,
                    description: $scope.clearDesc(data.item[i].description),
                    pubDate: data.item[i].pubDate
                };
            }
        } else if (data.entry) {
            for (var i = 0; i < data.entry.length; i++) {
                arr[i] = {
                    title: data.entry[i].title,
                    link: data.entry[i].link.href,
                    description: $scope.clearDesc(data.entry[i].summary.content),
                    pubDate: data.entry[i].updated
                };
            }
        }
        return arr;
    };
}]);

App.factory('FeedService', ['$http', function ($http) {
    return {
        requestFeed: function (url) {
            var yql = 'select * from feed where url = \'' + encodeURIComponent(url) + '\' limit ' + FeedsSource.config.maxItemsCount;
            var encyql = (yql);
            var params = '&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys';
            return $http.jsonp('http://query.yahooapis.com/v1/public/yql?q=' + encyql + '&format=json&callback=JSON_CALLBACK' + params);
        }
    }
}]);

angular.module('RSSFeedApp').filter('rssTitleSearch', function () {
    return function (items, searchString) {
        if (!searchString) {
            return items;
        }
        var filtered = [];

        angular.forEach(items, function (item) {
            if (item.title.toLocaleLowerCase().indexOf(searchString.toLocaleLowerCase()) !== -1) {
                filtered.push(item);
            }
        });
        return filtered;
    };
});
angular.module('RSSFeedApp')
    .directive('initPopover', function () {
        return {
            restrict: "A",
            link: function (scope, elem, attrs) {
                var options = {placement: scope.rssFeedsConfig.feedDescriptionPopoverPosition, trigger: "hover"};
                $(elem).popover(options);
            }
        }
    });