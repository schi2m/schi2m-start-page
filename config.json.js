/**
 * Created by schi2m on 2015-12-29.
 */
var FeedsSource = {
    "config": {
        retryDelay: 1000,
        maxRetryCount: 10,
        maxItemsCount: 20,
        feedDescriptionPopoverPosition: "auto left"
    },
    "ggStats":[
        {number:5618437, title:"Sylwo"},
        {number:3362080, title:"Sulek"},
        {number:3953673, title:"Daniel"},
        {number:2284067, title:"Atom"},
        {number:53433512, title:"Szymon"}
    ],
    "links": [
        {
            link: "https://news.google.pl/news/section?pz=1&cf=all&ned=pl_pl&topic=t&siidp=9dc3f22e6bd957b181f60f839c23015089d2&ict=ln",
            title: "Nauka i Technika"
        },
        {link: "http://webcamera.pl/", title: "Webcamera"},
        {link: "http://oognet.pl/", title: "Oognet"},
        {link: "http://warszawa.jakdojade.pl/", title: "Jak Dojad\u0119"},
        {link: "http://forum.maxthon.com/index.php?/forum/57-maxthon-official-releases-and-betas/", title: "Maxthon"},
        {link: "http://rsload.net/soft/", title: "RsLoad"},
        {link: "http://stri.ms/", title: "stri.ms"},
        {link: "http://www.wykop.pl/rss/", title: "Wykop"},
        {link: "http://sendfile.su/search/0/3/0/0/", title: "Send File"},
        {link: "http://www.2baksa.net/", title: "Gold Warez"},
        {link: "http://www.radio.pionier.net.pl/stream.pls?radio=radiotok", title: "TokFM"},
        {link: "http://www.alawar.pl/catalog/ukryte_obiekty/", title: "Alawar"},
    ],
    "feeds": [
        {
            title: "Gry | Exsite",
            url: "http://www.exsite.pl/gry_full_games/rss.xml",
            link: "http://www.exsite.pl/"
        }
        ,
        {
            title: "Chip - news",
            url: "http://www.chip.pl/rss/newsroom.rss",
            link: "http://www.chip.pl/"
        },
        {
            title: "Nowe | Dobre Programy",
            url: "http://feeds.feedburner.com/dobreprogramy/NoweWersjeStabilne",
            link: "http://www.dobreprogramy.pl/"
        },
        {
            title: "TVN24",
            url: "http://www.tvn24.pl/najwazniejsze.xml",
            link: "http://www.tvn24.pl/"
        },
        {
            title: "Instalki ",
            url: "http://www.instalki.pl/rss/nowosci.php",
            link: "http://www.instalki.pl/"
        },
        {
            title: "strims",
            url: "http://strims.pl/rss",
            link: "http://strims.pl/"
        },
        {
            title: "PCLab.pl",
            url: "http://pclab.pl/xml/aktualnosci.xml",
            link: "http://pclab.pl"
        },
        {
            title: "File Forum",
            url: "http://feeds.fileforum.com/fileforum/full?format=xml",
            link: "http://fileforum.com/"
        },
        {
            title: "Google News",
            url: "http://news.google.pl/news?pz=1&cf=all&ned=pl_pl&hl=pl&output=rss",
            link: "https://news.google.pl/"
        },
        {
            title: "Benchmark",
            url: "http://www.benchmark.pl/rss/aktualnosci-pliki.xml",
            link: "http://www.benchmark.pl/"
        }
    ]
};